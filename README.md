## Welcome to Covidopedia! 

```
I'm a simple chat bot that can answer your question about Covid-19.
I'm using WHO QA datas as the main of my chatbot knowledge and you better to check it out.
On top of that, I'm also using levenshtein distance algorithm for calculating the similarity between two questions.

Reference: https://www.who.int/indonesia/news/novel-coronavirus/qa
```

## Run all services

```
docker-compose up
```

## Here are some demo videos about this app:

![Demo 1](documentation/demo1.mov)
![Demo 2](documentation/demo2.mov)
